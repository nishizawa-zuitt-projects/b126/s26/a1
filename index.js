const express = require ("express")
const mongoose = require("mongoose")
const app = express();


mongoose.connect("mongodb+srv://admin:admin@cluster0.aq4ae.mongodb.net/b126_to-do?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"))

const taskSchema = new mongoose.Schema({ 
	name: String, 
	status: {
		type: String, 
		default: "pending"
	}
})

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const Task = mongoose.model("Task", taskSchema)
const User = mongoose.model("User", userSchema)

app.use(express.json()); 
app.use(express.urlencoded({
	extended:true
}))

const port = 4000;

app.get("/", (req, res) => {
	res.send("Hello Hiro from Express!")
})

app.post("/tasks", (req,res) =>{
	console.log(req.body)

	Task.findOne({name: req.body.name}, (err, result) => {
		console.log(result)
		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate task found")
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((err,task)=> {
				if(err){
					return res.send("Error creating new task")
				}else{
					return res.send("New task successfully created")
				}
			})
		}
	})
})

app.get("/tasks", (req,res) => {

	Task.find({}, (err, result) =>{
		
		if(err){
			return res.send("Error getting tasks.")
		}else{
			return res.json({
				data: result
			})
		}
	})
})

// The Anser for the activity is below:
app.post("/users", (req, res) =>{
	console.log(req.body)

	if (req.body.username == "" || req.body.password == "" || 
	   (req.body.username == "" && req.body.password == "")){
	   	return res.send("Error: you need to set both of username and password")

	}else{
		User.findOne({username: req.body.username}, (err, result) =>{
			if(result !== null && result.username === req.body.username){
				return res.send("Duplicate username found")
			}else{
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})

				newUser.save((err,user)=> {
					if(err){
						return res.send("Error creating new user")
					}else{
						return res.send("New user record successfully created")
					}
				})
			}
		})
	}
})

app.listen(port, () => console.log(`Server running at port ${port}`))


/*
Activity:
	Create a route for creating a new user when a POST request is sent to the /users
	endpoint. If either the username or password fields are empty, registration will 
	not take place (return an error message). After that, check for duplicate usernames 
	before creating a new user.
*/

















